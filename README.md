# YourWastedTime #

### What is it? ###

YourWastedTime is a small app for WP 8.1. The purpose of application is check how much time user wasted on watching TV Shows. Application uses with API that is sharing by TheMovieDb.org. User has opportunity to save, load, remove and share results.

### Utilised technologies ###

* C#
* JSON.NET

### Screenshots ###

* [Main page](http://i.imgur.com/pSfI3n9.png)
* [Search results](http://i.imgur.com/HCgtrzK.png)
* [Adding tv show](http://i.imgur.com/ovLVc7x.png)