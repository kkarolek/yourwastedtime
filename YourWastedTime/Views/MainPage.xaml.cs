﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourWastedTime.Model;
using YourWastedTime.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace YourWastedTime
{
    public sealed partial class MainPage : Page
    {
        private MainViewModel mvm;
        private StoringAndRetrieving storingAndRetrieving;
        private DataTransferManager dataTransferManager;

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            mvm = new MainViewModel();
            DataContext = mvm;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var parameter = e.Parameter as MainViewModel;
            if(parameter != null)
            {
                mvm = e.Parameter as MainViewModel;
                TimeToString(CalculateTotalTime());
            }
            this.dataTransferManager = DataTransferManager.GetForCurrentView();
            this.dataTransferManager.DataRequested += 
                new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {
            if (GetShareContent(e.Request))
            {
                if (String.IsNullOrEmpty(e.Request.Data.Properties.Title))
                {
                    e.Request.FailWithDisplayText("Title is required.");
                }
            }
        }

        private bool GetShareContent(DataRequest request)
        {
            bool succeeded = false;

            StringBuilder sb = new StringBuilder();
            foreach(var tvShow in mvm.TvShows)
            {
                sb.Append(tvShow.Title);
                sb.Append(" - # of seen seasons: ");
                sb.Append(tvShow.NumberOfSeenSeasons);
                sb.Append(Environment.NewLine);
            }
            sb.Append("Total wasted time: ");
            sb.Append(mvm.TotalWastedTime);

            if (!String.IsNullOrEmpty(sb.ToString()))
            {
                DataPackage requestData = request.Data;
                requestData.Properties.Title = "My wasted time spent watching TV shows.";
                requestData.SetText(sb.ToString());
                succeeded = true;
            }
            else
            {
                request.FailWithDisplayText("Enter the text you would like to share and try again.");
            }
            return succeeded;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(TvShowsList), mvm);
        }

        private void GridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gridView.SelectedIndex == -1)
                return;
            var item = e.AddedItems[0] as TvShow;
            if (item != null)
            {
                mvm.TvShows.Remove(item);
                TimeToString(CalculateTotalTime());
            }
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            storingAndRetrieving = new StoringAndRetrieving();
            try
            {
                await storingAndRetrieving.writeJsonAsync(mvm.TvShows);
                MessageDialog messageDialog = new MessageDialog("Saved!");
                messageDialog.ShowAsync();
            }
            catch(Exception ex)
            {
                MessageDialog messageDialog = new MessageDialog("Error." + ex);
                messageDialog.ShowAsync();
            }
        }

        private async void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            ObservableCollection<TvShow> collection = new ObservableCollection<TvShow>();
            storingAndRetrieving = new StoringAndRetrieving();
            try
            {
                collection = await storingAndRetrieving.deserializeJsonAsync();
                foreach (var item in collection)
                {
                    mvm.TvShows.Add(item);
                }
                TimeToString(CalculateTotalTime());
            }
            catch(Exception)
            {
                MessageDialog messageDialog = new MessageDialog("Data not found.");
                messageDialog.ShowAsync();
            }
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private void TimeToString(double time)
        {
            int days = (int)Math.Floor(time / 1440);
            int hours = (int)Math.Floor((time - days * 1440) / 60);
            int minutes = (int)Math.Floor(time - days * 1440 - hours * 60);
            mvm.TotalWastedTime = String.Format("{0}d {1}h {2}m", days, hours, minutes);
        }

        private double CalculateTotalTime()
        {
            double time = 0;
            foreach (var tvShow in mvm.TvShows)
            {
                time += tvShow.CalculateWastedTime();
            }
            return time;
        }
    }

}
