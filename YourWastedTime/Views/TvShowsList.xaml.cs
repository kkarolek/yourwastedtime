﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YourWastedTime.Model;
using YourWastedTime.ViewModel;

namespace YourWastedTime
{
    public sealed partial class TvShowsList : Page
    {
        private ObservableCollection<TvShow> FoundTvShows;
        private MainViewModel mvm;
        private Popup popup;
        public TvShowsList()
        {
            this.InitializeComponent();
            FoundTvShows = new ObservableCollection<TvShow>();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            mvm = e.Parameter as MainViewModel;
            if(mvm.MovieDbApi.Term!="")
            {
                try
                {
                    FoundTvShows = await mvm.MovieDbApi.Search();
                    if(FoundTvShows.Count == 0)
                    {
                        MessageDialog messageDialog = new MessageDialog("No results.");
                        await messageDialog.ShowAsync();
                        Frame.GoBack();
                    }
                    else
                    {
                        DataContext = FoundTvShows;
                    }
                }
                catch (Exception ex)
                {
                    MessageDialog messageDialog = new MessageDialog("No results." + ex);
                    messageDialog.ShowAsync();
                }
            }
            mvm.MovieDbApi.Term = "";
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }


        private async void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var clickedTvShow = e.AddedItems[0] as TvShow;
            TvShow tvShow = await mvm.MovieDbApi.Get(clickedTvShow.Id);
            popup = new Popup();
            PopUpUserControl popUpUserControl = new PopUpUserControl();
            popup.Child = popUpUserControl;
            popUpUserControl.Width = Window.Current.Bounds.Width;
            popup.DataContext = tvShow;
            popup.IsOpen = true;

            popUpUserControl.okBtn.Click += (s, args) =>
                {
                    if(tvShow.NumberOfSeenSeasons > 0)
                    {
                        popup.IsOpen = false;
                        mvm.TvShows.Add(tvShow);
                        Frame.Navigate(typeof(MainPage), mvm);
                    }

                };
            popUpUserControl.cancelBtn.Click += (s, args) =>
                {
                    popup.IsOpen = false;
                };
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if(popup != null && popup.IsOpen)
            {
                popup.IsOpen = false;
            }
            else
            {
                Frame frame = Window.Current.Content as Frame;
                if (frame != null && frame.CanGoBack)
                {
                    frame.GoBack();
                }
            }
            e.Handled = true;
        }

    }
}
