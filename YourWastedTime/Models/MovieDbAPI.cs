﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace YourWastedTime.Model
{
    public class MovieDbAPI : INotifyPropertyChanged
    {
        private const string ApiKey = "1d7a69674305f804e0a5401cce27ed48";
        private string _term;

        public string Term
        {
            get { return _term; }
            set { _term = value; Changed(); }
        }

        public async Task<ObservableCollection<TvShow>> Search()
        {
            ObservableCollection<TvShow> collection = new ObservableCollection<TvShow>();
            HttpClient http = new HttpClient();
            HttpResponseMessage response = await http.GetAsync("http://api.themoviedb.org/3/search/tv?query="+Term+"&api_key="+ApiKey);
            var webResponse = await response.Content.ReadAsStringAsync();
            
            JObject jObject = JObject.Parse(webResponse);
            JArray results = (JArray)jObject["results"];
            foreach (JToken result in results)
            {
                var id = (int)result["id"];
                var title = (string)result["original_name"];
                string poster = "https://image.tmdb.org/t/p/original" + (string)result["poster_path"];
                TvShow tvShow = new TvShow() { Id = id, Title = title, PosterPath = poster };
                collection.Add(tvShow);
            }
            return collection;
        }

        public async Task<TvShow> Get(int id)
        {
            HttpClient http = new HttpClient();
            HttpResponseMessage response = await http.GetAsync("https://api.themoviedb.org/3/tv/"+id+"?api_key="+ApiKey);
            var webResponse = await response.Content.ReadAsStringAsync();

            JObject jObject = JObject.Parse(webResponse);
            JArray episodeRunTime = (JArray)jObject["episode_run_time"];
            List<int> list = episodeRunTime.Select(o => (int)o).ToList();
            int numberOfEpisodes = (int)jObject["number_of_episodes"];
            int numberOfSeasons = (int)jObject["number_of_seasons"];
            string title = (string)jObject["original_name"];
            string poster = "https://image.tmdb.org/t/p/original" + (string)jObject["poster_path"];
            TvShow tvShow = new TvShow()
            {
                NumberOfEpisodes = numberOfEpisodes,
                NumberOfSeasons = numberOfSeasons,
                PosterPath = poster,
                Title = title,
                Id = id,
                EpisodeRunTime = list
            };
            return tvShow;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Changed([CallerMemberName]string propertyName=null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
