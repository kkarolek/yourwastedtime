﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using System.IO;


namespace YourWastedTime.Model
{
    public class StoringAndRetrieving
    {
        private const string JSONFILENAME = "data.json";

        public async Task writeJsonAsync(ObservableCollection<TvShow> tvShowList)
        {
            var serializer = new DataContractJsonSerializer(typeof(ObservableCollection<TvShow>));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, tvShowList);
            }
        }

        public async Task<ObservableCollection<TvShow>> deserializeJsonAsync()
        {
            ObservableCollection<TvShow> tvShowList;
            var jsonSerializer = new DataContractJsonSerializer(typeof(ObservableCollection<TvShow>));

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

            tvShowList = (ObservableCollection<TvShow>)jsonSerializer.ReadObject(myStream);

            return tvShowList;
        }
    }
}
