﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YourWastedTime.Model
{
    public class TvShow
    {
        private int _id;
        private string _title;
        private List<int> _episodeRunTime;
        private int _numberOfEpisodes;
        private int _numberOfSeasons;
        private int _numberOfSeenSeasons;
        private string _posterPath;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public List<int> EpisodeRunTime
        {
            get
            {
                if(_episodeRunTime == null)
                {
                    _episodeRunTime = new List<int>();
                }
                return _episodeRunTime;
            }
            set { _episodeRunTime = value; }
        }

        public int NumberOfEpisodes
        {
            get { return _numberOfEpisodes; }
            set { _numberOfEpisodes = value; }
        }

        public int NumberOfSeasons
        {
            get { return _numberOfSeasons; }
            set { _numberOfSeasons = value; }
        }

        public int NumberOfSeenSeasons
        {
            get { return _numberOfSeenSeasons; }
            set { _numberOfSeenSeasons = value; }
        }

        public string PosterPath
        {
            get { return _posterPath; }
            set { _posterPath = value; }
        }

        public double CalculateWastedTime()
        {
            double time = NumberOfSeenSeasons * (NumberOfEpisodes / NumberOfSeasons) * EpisodeRunTime.Average();
            return time;
        }
    }
}
