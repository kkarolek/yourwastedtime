﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using YourWastedTime.Model;

namespace YourWastedTime.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            TvShows = new ObservableCollection<TvShow>();
            MovieDbApi = new MovieDbAPI();
        }

        private string _totalWastedTime;
        public ObservableCollection<TvShow> TvShows { get; set; }
        public MovieDbAPI MovieDbApi { get; set; }
        public string TotalWastedTime
        {
            get
            {
                return _totalWastedTime;
            }
            set
            {
                _totalWastedTime = value;
                Changed();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Changed([CallerMemberName]string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
